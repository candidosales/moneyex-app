import { CryptocurrencyService } from './../../providers/cryptocurrency.service';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { Observable } from 'rxjs/Observable';
import { map, filter } from 'rxjs/operators';


// Observable operators
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import "rxjs/add/observable/interval";


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {

  public codeCrypto: Array<string> = [
    'btc-usd',
    'etc-usd',
    'eth-usd',
    'xrp-usd',
    'iota-usd',
    'ltc-usd'
  ];

  public responsesCode: Array<any> = [];

  public cryptoCurrencies$: Observable<any>;

  constructor(public navCtrl: NavController,
              public cryptocurrencyService: CryptocurrencyService) {

  }

  ngOnInit(): void {
    this.getCryptocurrencies(10000);
  }

  public getCryptocurrencies(timer) {
    setInterval(() => {
      this.cryptocurrencyService
      .getMultiple(this.codeCrypto)
      .subscribe(response => {
        console.log(response);

        if (response) {
          console.log(response);
          this.cryptoCurrencies$ = Observable.of(response
          .filter(response => response.success == true)
          .map(response => response.ticker));
        }
      }, error => {
        console.log(error);
      });
    }, timer);
  }

  public itemSelected(item) {
    console.log(item);
  }

}
