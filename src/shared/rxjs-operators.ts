// Observable class extensions
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/observable/of';

import { map, filter, catchError } from 'rxjs/operators';

