import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';

@Injectable()
export class HttpClient {
  private options: RequestOptions;

  constructor(public http: Http) {
    this.http = http;
    this.options = this.createRequestOptions();
  }

  public createRequestOptions(): RequestOptions {
    const options = new RequestOptions();
    // const headers = new Headers();

    // headers.append('Content-type', 'application/json');

    // options.headers = headers;

    // Security
    // options.withCredentials = true;

    // 1EFuToNTXoAusYUryniHRq35g8pPoeMGco

    return options;
  }

  public get(url): Observable<Response> {
    return this.http.get(url, this.options).pipe(
                map(this.extractData),
                catchError(this.handleError));
  }

  public post(url, data): Observable<Response> {
    return this.http.post(url, data, this.options).pipe(
      map(this.extractData),
      catchError(this.handleError));
  }

  public put(url, data): Observable<Response> {
    return this.http.put(url, data, this.options).pipe(
      map(this.extractData),
      catchError(this.handleError));
  }

  public delete(url): Observable<Response> {
    return this.http.delete(url, this.options).pipe(
      map(this.extractData),
      catchError(this.handleError));
  }

  public setOptions(options: RequestOptions): Http {
    this.options = options;
    return this.http;
  }

  private extractData(res: Response) {
    let body = res.json();
    return body || { };
  }

  private handleError (error: Response | any) {
  // In a real world app, we might use a remote logging infrastructure
  let errMsg: string;
  if (error) {
    try {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      const errorMessages = [];

      const errObject = JSON.parse(err);
      for (var key in errObject) {
        errorMessages.push(key + ': ' + errObject[key].join(', '));
      }

      errMsg = `${error.status} - ${error.statusText || ''} - ${errorMessages}`;
    } catch (err) {
      const body = error.json() || '';
      errMsg = body.message ? body.message : error.toString();
    }
  } else {
    errMsg = error.message ? error.message : error.toString();
  }
  console.error(errMsg);
  return Promise.reject(errMsg);
}

}
