import { Injectable } from '@angular/core';
import { HttpClient } from './http-client';
import { RequestOptions, Headers } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { map, filter } from 'rxjs/operators';
import 'rxjs/add/observable/forkJoin';

/*
  Generated class for the Login provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class CryptocurrencyService {

  private url = `https://api.cryptonator.com/api/ticker`;

  constructor(public httpClient: HttpClient) {
    // this.defineHeaders();
  }

  public defineHeaders() {
    const options = new RequestOptions();
    const headers = new Headers();

    // headers.append('Authorization', 'Bearer ' + token);
    // headers.append('Accept', 'application/vnd.genres.v1+json');

    options.headers = headers;

    this.httpClient.setOptions(options);

  }

  public get(code: any): Observable<any> {
    return this.httpClient.get(`${this.url}/${code}`);
  }

  public getMultiple(codes: Array<string>): Observable<any> {
    const responsesCode: Array<any> = [];

    codes.forEach((code) => {
      responsesCode.push(this.get(code));
    });

    return Observable
            .forkJoin(responsesCode).map(responses => {
              return [].concat(...responses);
            });
  }
}
